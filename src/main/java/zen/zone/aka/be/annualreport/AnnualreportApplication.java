package zen.zone.aka.be.annualreport;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AnnualreportApplication {

	public static void main(String[] args) {
		SpringApplication.run(AnnualreportApplication.class, args);
	}
}
