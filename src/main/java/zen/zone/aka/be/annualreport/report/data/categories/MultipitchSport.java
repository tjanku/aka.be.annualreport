package zen.zone.aka.be.annualreport.report.data.categories;

import lombok.Data;
import zen.zone.aka.be.annualreport.report.data.types.StyleType;

import java.util.List;

@Data
public class MultipitchSport implements ReportEntry {

    private String name;
    private List<String> classification;
    private String sector;
    private String region;
    private StyleType style;

}
