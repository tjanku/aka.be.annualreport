package zen.zone.aka.be.annualreport.report.data.categories;

import lombok.Data;

@Data
public class Other implements ReportEntry {
    private String info;
}
