package zen.zone.aka.be.annualreport.report.data;

import lombok.Builder;
import lombok.Data;
import zen.zone.aka.be.annualreport.report.data.categories.ReportEntry;
import zen.zone.aka.be.annualreport.report.data.types.ReportType;

import java.util.List;
import java.util.Map;

@Data
@Builder
public class Report {

    private int year;
    private String username;

    private Map<ReportType, List<ReportEntry>> entries;

}

