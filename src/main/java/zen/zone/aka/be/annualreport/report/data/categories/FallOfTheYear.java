package zen.zone.aka.be.annualreport.report.data.categories;

import lombok.Data;

@Data
public class FallOfTheYear implements ReportEntry {

    private String who;
    private String where;
    private int height;
    private String info;

}
