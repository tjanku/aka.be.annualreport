package zen.zone.aka.be.annualreport.report;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import zen.zone.aka.be.annualreport.report.data.Report;

import java.lang.invoke.MethodHandles;
import java.util.Arrays;
import java.util.List;

@Component
public class ReportServiceImpl implements ReportService {

    private static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Override
    public List<Report> reports() {
        LOG.info("Getting all reports");

        return Arrays.asList(
                Report.builder().year(2017).username("test@test.cz").build(),
                Report.builder().year(2017).username("test2@test.cz").build()
        );
    }

    @Override
    public List<Report> reportsOfUser(String username) {
        LOG.info("Getting all reports for user {}", username);

        return Arrays.asList(
                Report.builder().year(2017).username("test@test.cz").build()
        );
    }

}
