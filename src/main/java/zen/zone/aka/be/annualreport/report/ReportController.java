package zen.zone.aka.be.annualreport.report;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import zen.zone.aka.be.annualreport.report.data.Report;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.util.List;

@Controller
public class ReportController {

    private ReportService reportService;

    public ReportController(ReportService reportService) {
        this.reportService = reportService;
    }

    @GetMapping(value = "/reports", produces = "application/json")
    List<Report> reports() {
        return reportService.reports();
    }

    @GetMapping(value = "/reports/user/{username}")
    List<Report> reportsOfUser(@NotNull @Email String username) {
        return reportService.reportsOfUser(username);
    }

}
