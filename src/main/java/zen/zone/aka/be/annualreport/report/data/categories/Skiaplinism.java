package zen.zone.aka.be.annualreport.report.data.categories;

import lombok.Data;

@Data
public class Skiaplinism implements ReportEntry {

    private String name;
    private String classification;
    private int slope;
    private int length;
    private String mountain;
    private int height;
    private String region;
    private String otherInfo;

}
