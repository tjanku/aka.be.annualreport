package zen.zone.aka.be.annualreport.report.data.types;

public enum SandstoneType {
    TOWER,
    MASSIVE
}
