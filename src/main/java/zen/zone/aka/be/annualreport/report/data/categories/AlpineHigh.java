package zen.zone.aka.be.annualreport.report.data.categories;

import lombok.Data;

@Data
public class AlpineHigh implements ReportEntry {

    private String name;
    private String classification;
    private String mountain;
    private int height;
    private String region;
    private String otherInfo;

}
