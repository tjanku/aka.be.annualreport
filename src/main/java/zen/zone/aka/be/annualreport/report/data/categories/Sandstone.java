package zen.zone.aka.be.annualreport.report.data.categories;

import lombok.Data;
import zen.zone.aka.be.annualreport.report.data.types.SandstoneType;
import zen.zone.aka.be.annualreport.report.data.types.StyleType;

@Data
public class Sandstone implements ReportEntry {
    private String name;
    private String classification;
    private SandstoneType type;
    private String region;
    private StyleType style;
}
