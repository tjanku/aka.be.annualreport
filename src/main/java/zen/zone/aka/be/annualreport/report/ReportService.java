package zen.zone.aka.be.annualreport.report;

import zen.zone.aka.be.annualreport.report.data.Report;

import java.util.List;

public interface ReportService {

    /**
     * Get all reports
     *
     * @return
     */
    List<Report> reports();

    /**
     * Get reports for the current user
     *
     * @param username
     * @return
     */
    List<Report> reportsOfUser(String username);


}
