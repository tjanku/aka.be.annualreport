package zen.zone.aka.be.annualreport.report.data.categories;

import lombok.Data;

@Data
public class Bouldering implements ReportEntry {

    private String name;
    private String classification;
    private String sector;
    private String region;

}
