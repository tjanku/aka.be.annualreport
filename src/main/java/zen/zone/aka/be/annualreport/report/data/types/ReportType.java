package zen.zone.aka.be.annualreport.report.data.types;

import lombok.Getter;
import zen.zone.aka.be.annualreport.report.data.categories.AlpineHigh;
import zen.zone.aka.be.annualreport.report.data.categories.AlpineMid;
import zen.zone.aka.be.annualreport.report.data.categories.Bigwall;
import zen.zone.aka.be.annualreport.report.data.categories.Bouldering;
import zen.zone.aka.be.annualreport.report.data.categories.FallOfTheYear;
import zen.zone.aka.be.annualreport.report.data.categories.IceDrytool;
import zen.zone.aka.be.annualreport.report.data.categories.MultipitchSport;
import zen.zone.aka.be.annualreport.report.data.categories.Other;
import zen.zone.aka.be.annualreport.report.data.categories.Sandstone;
import zen.zone.aka.be.annualreport.report.data.categories.Skiaplinism;
import zen.zone.aka.be.annualreport.report.data.categories.Sport;
import zen.zone.aka.be.annualreport.report.data.categories.Traditional;

@Getter
public enum ReportType {
    ALPINEHIGH(AlpineHigh.class),
    ALPINEMID(AlpineMid.class),
    BIGWALL(Bigwall.class),
    BOULDERING(Bouldering.class),
    FALLOFTHEYEAR(FallOfTheYear.class),
    ICEDRYTOOL(IceDrytool.class),
    MULTIPITCHSPORT(MultipitchSport.class),
    OTHER(Other.class),
    SANDSTONE(Sandstone.class),
    SKIAPLINISM(Skiaplinism.class),
    SPORT(Sport.class),
    TRADITIONAL(Traditional.class);

    private Class reportClass;

    ReportType(Class reportClass) {
        this.reportClass = reportClass;
    }
}
