package zen.zone.aka.be.annualreport.report.data.categories;

import lombok.Data;
import zen.zone.aka.be.annualreport.report.data.types.StyleType;

@Data
public class Sport implements ReportEntry {

    private String name;
    private String classification;
    private String region;
    private StyleType style;

}
