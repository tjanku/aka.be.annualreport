package zen.zone.aka.be.annualreport.report.data.types;

public enum StyleType {
    OS,
    FLASH,
    RP,
    PP,
    RK,
    AF,
    TR,
    FREESOLO,
    DWS
}
